CREATE TABLE IF NOT EXISTS playerdata (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, player_uuid VARCHAR(32),
kills INT, assists INT, deaths INT, wins INT, fire_kills INT, fire_assists INT, fire_deaths INT, ice_kills INT,
  ice_assists INT, ice_deaths INT, wither_kills INT, wither_assists INT, wither_deaths INT, kinetci_kills INT,
  kinetic_assists INT, kinetic_deaths INT, blood_kills INT, blood_assists INT, blood_deaths INT, toxic_kills INT,
  toxic_assists INT, toxic_deaths INT, lookup_time INT)

CREATE TABLE IF NOT EXISTS playerlookup (player_uuid VARCHAR(36) PRIMARY KEY,
  weekly_lookup BOOL, daily_lookup BOOL)