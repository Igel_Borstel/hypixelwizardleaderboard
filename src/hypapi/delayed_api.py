from . import hypixelapi
import time

current_milli_time = lambda: int(round(time.time() * 1000))


class DelayedApi:

    API_KEY = ""
    last_request = 0

    hypixel_api = None
    def __init__(self):
        if self.hypixel_api is None:
            self.hypixel_api = hypixelapi.HypixelApi()
            self.last_request = current_milli_time()

    def set_api_key(self, key):
        self.API_KEY = key
        self.hypixel_api.set_api_key(self.API_KEY)


    def get_api_key(self):
        return self.API_KEY

    def do_request(self, req):
        cur_time = current_milli_time()
        if (cur_time - self.last_request) < 500:
            time.sleep(0.5)
        self.last_request = cur_time
        return self.hypixel_api.do_request(req)



