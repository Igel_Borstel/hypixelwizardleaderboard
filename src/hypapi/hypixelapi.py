import json
import urllib.request

from hypapi.requesting import request as req


class HypixelApi:
    API_KEY = None

    def set_api_key(self, key):
        self.API_KEY = key

    def get_api_key(self):
        return self.API_KEY

    def do_request(self, request):
        if type(request) is not req.Request:
            raise TypeError("Type not supported as request.")
        try:
            url = request.get_url()
            url = str(url)
            url = url.replace("$HYPIXELAPIKEY$", self.API_KEY)
            request_url = urllib.request.Request(url,
                                                 headers={"User-Agent": "Mozilla/5.0 (Windows Nt 6.1; Win64; x64)"})
            return json.loads(urllib.request.urlopen(request_url).read())
        except RuntimeError as error:
            raise error #we can't do anything; the caller give us a nonvalid request -> he has to do some work
