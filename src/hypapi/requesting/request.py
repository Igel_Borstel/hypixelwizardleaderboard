from . import request_type as req_type
from . import request_parameter


class Request:
    BASE_URL = "https://api.hypixel.net/"

    def __init__(self, request_type=None, paras=[]):
        if request_type is not None and type(request_type) is not req_type.RequestType:
            raise TypeError("Not supported request_type!")

        for parameter in paras:
            if type(parameter) is not request_parameter.RequestParameter:
                raise TypeError("Not supported request parameter type found!")

        self.request_type = request_type
        self.parameters = paras

    def set_request_type(self, request_type):
        if request_type is not None and type(request_type) is not req_type.RequestType:
            raise TypeError("Not supported request_type!")
        self.request_type = request_type

    def add_request_parameter(self, parameter):
        self.add_request_parameters([parameter])

    def add_request_parameters(self, paras):
        for parameter in paras:
            if type(parameter) is not request_parameter.RequestParameter:
                raise TypeError("Not supported request parameter type found!")
            self.parameters.append(parameter)

    def get_url(self):
        if self.request_type is None:
            raise RuntimeError("no request type was given")
        url = self.BASE_URL
        url += self.request_type.value
        url += "?key=$HYPIXELAPIKEY$&"
        for parameter in self.parameters:
            url += parameter.get_request_parameter_type().value + "=" + parameter.get_payload() + "&"
        return url
