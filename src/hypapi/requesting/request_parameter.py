import enum


class RequestParameterType(enum.Enum):
    KEY = "key"
    PLAYER_BY_NAME = "name"
    PLAYER_BY_UUID = "uuid"
    GUILD_BY_NAME = "byName"
    GUILD_BY_UUID = "byUuid"
    GUILD_BY_ID = "id"
    FRIENDS_BY_UUID = "uuid"
    SESSION_BY_UUID = "uuid"


class RequestParameter:
    def __init__(self, request_parameter_type, payload):
        if type(request_parameter_type) is not RequestParameterType:
            raise TypeError("only RequestParameterType is a supported Type for RequestParameter")
        self.reqType = request_parameter_type
        self.payload = payload

    def get_request_parameter_type(self):
        return self.reqType

    def get_payload(self):
        return self.payload