import enum


class RequestType(enum.Enum):
    PLAYER = "player"
    GUILD = "guild"
    FIND_GUILD = "findGuild"
    FRIENDS = "friends"
    BOOSTERS = "boosters"
    LEADERBOARDS = "leaderboards"
    SESSION = "session"
    KEY = "key"
    PLAYER_COUNT = "playerCount"
    WATCHDOG_STATS = "watchdogStats"
