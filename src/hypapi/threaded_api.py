from . import hypixelapi
import queue
import threading
import time


class CachedApi:

    API_KEY = ""
    MAX_REQUEST_PER_MINUTE = 120
    DELTA_TIME_MINUTE = 60

    hypixel_api = None
    request_queue = queue.Queue()
    thread = None

    def __init__(self):
        if self.hypixel_api is None:
            self.hypixel_api = hypixelapi.HypixelApi()
            self.thread = threading.Thread(target=self.do_requests)
            self.thread.active = True
            self.thread.start()

    def set_api_key(self, key):
        self.API_KEY = key
        self.hypixel_api.set_api_key(self.API_KEY)


    def get_api_key(self):
        return self.API_KEY

    def queue_request(self, req, callback):
        self.request_queue.put((req, callback))
        pass

    def stop_execution(self):
        self.thread.active = False

    def do_requests(self):
        current_request_number = 0
        total_request = 1
        current_time = time.time()
        current_thread = threading.current_thread()
        while current_thread.active:
            if int(time.time()) - int(current_time) < self.DELTA_TIME_MINUTE and current_request_number < self.MAX_REQUEST_PER_MINUTE:
                if not self.request_queue.empty():
                    request = self.request_queue.get_nowait()
                    request[1](self.hypixel_api.do_request(request[0]))
                    current_request_number += 1
                    total_request += 1
            else:
                current_request_number = 0
                current_time = time.time()
            time.sleep(0.01)




