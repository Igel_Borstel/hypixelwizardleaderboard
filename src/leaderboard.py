import re

import request_parser
from flask import Flask, url_for, render_template, redirect, request
from flask_wtf import FlaskForm
from hypapi.requesting import request as hyprequest
from hypapi.requesting import request_type
from wtforms import StringField
from wtforms.validators import DataRequired, Length

from hypapi import delayed_api
from hypapi.requesting import request_parameter

app = Flask(__name__)
app.config.from_pyfile("wizardlist.cfg")

hypixel_api = None


class MyForm(FlaskForm):
    name = StringField("name/uuid", validators=[DataRequired(), Length(min=3, max=36)])



#@app.before_first_request
def init_wizardlist():
    global hypixel_api
    hypixel_api = delayed_api.DelayedApi()
    hypixel_api.set_api_key(app.config["HYPIXEL_API_KEY"])

init_wizardlist()

@app.route("/playerbyuuid/<string:uuid>")
def show_user_by_uuid(uuid):
    req = hyprequest.Request(request_type=request_type.RequestType.PLAYER, paras=[
        request_parameter.RequestParameter(request_parameter.RequestParameterType.PLAYER_BY_UUID, uuid)])
    query_result = hypixel_api.do_request(req)
    if not query_result["success"]:
        return "Ups! You found an error. Please report the error!"
    else:
        if query_result["player"] is not None:
            parsed_data = request_parser.parse_single_player(query_result)
            return render_template("single_player_result.html", wizard_data=parsed_data["wizard_data"],
                                   class_data=parsed_data["class_data"])
        else:
            return render_template("single_player_result_player_not_found.html")

@app.route("/playerbyname/<string:name>")
def show_user_by_name(name):
    req = hyprequest.Request(request_type=request_type.RequestType.PLAYER, paras=[
        request_parameter.RequestParameter(request_parameter.RequestParameterType.PLAYER_BY_NAME, name)])
    query_result = hypixel_api.do_request(req)
    if not query_result["success"]:
        return "Ups! You found an error. Please report the error!"
    else:
        if query_result["player"] is not None:
            parsed_data = request_parser.parse_single_player(query_result)
            return render_template("single_player_result.html", wizard_data=parsed_data["wizard_data"],
                                   class_data=parsed_data["class_data"])
        else:
            return render_template("single_player_result_player_not_found.html")

@app.route("/searchsingleplayer", methods=("GET", "POST"))
def search_single_player():
    form = MyForm()
    if request.method == "POST" and form.validate_on_submit():
        regex = re.compile("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")
        if regex.match(form.name.data):
            return show_user_by_uuid(form.name.data)
        else:
            return show_user_by_name(form.name.data)
    return render_template("single_player_form.html", form=form)

@app.route("/")
def show_index():
    return redirect(url_for("search_single_player"))


if __name__ == "__main__":
    app.run()
