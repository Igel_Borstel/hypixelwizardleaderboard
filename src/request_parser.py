
def getValue(key, dict):
    if key in dict:
        return dict[key]
    else:
        return 0


def parse_single_player(query_result):
    wizard_data = {}
    wizard_data["player_name"] = query_result["player"]["displayname"]
    wizard_data["uuid"]=query_result["player"]["uuid"]
    wizard_data["kills"] = getValue("kills_capture", query_result["player"]["stats"]["TNTGames"])
    wizard_data["assists"] = getValue("assists_capture", query_result["player"]["stats"]["TNTGames"])
    wizard_data["deaths"] = getValue("deaths_capture", query_result["player"]["stats"]["TNTGames"])
    wizard_data["wins"] = getValue("wins_capture", query_result["player"]["stats"]["TNTGames"])
    wizard_data["selected_class"] = getValue("wizards_selected_class", query_result["player"]["stats"]["TNTGames"])
    class_data = []
    fire_class = {}
    fire_class["class"] = "fire"
    fire_class["kills"] = getValue("new_firewizard_kills", query_result["player"]["stats"]["TNTGames"])
    fire_class["assists"] = getValue("new_firewizard_assists", query_result["player"]["stats"]["TNTGames"])
    fire_class["deaths"] = getValue("new_firewizard_deaths", query_result["player"]["stats"]["TNTGames"])
    fire_class["explosion"] = getValue("new_firewizard_explode", query_result["player"]["stats"]["TNTGames"])
    fire_class["regeneration"] = getValue("new_firewizard_regen", query_result["player"]["stats"]["TNTGames"])
    fire_class["prestige"] = str("new_firewizard_prestige_field" in query_result["player"]["stats"]["TNTGames"])
    class_data.append(fire_class)
    ice_class = {}
    ice_class["class"] = "ice"
    ice_class["kills"] = getValue("new_icewizard_kills", query_result["player"]["stats"]["TNTGames"])
    ice_class["assists"] = getValue("new_icewizard_assists", query_result["player"]["stats"]["TNTGames"])
    ice_class["deaths"] = getValue("new_icewizard_deaths", query_result["player"]["stats"]["TNTGames"])
    ice_class["explosion"] = getValue("new_icewizard_explode", query_result["player"]["stats"]["TNTGames"])
    ice_class["regeneration"] = getValue("new_icewizard_regen", query_result["player"]["stats"]["TNTGames"])
    ice_class["prestige"] = str("new_icewizard_prestige_field" in query_result["player"]["stats"]["TNTGames"])
    class_data.append(ice_class)
    wither_class = {}
    wither_class["class"] = "wither"
    wither_class["kills"] = getValue("new_witherwizard_kills", query_result["player"]["stats"]["TNTGames"])
    wither_class["assists"] = getValue("new_witherwizard_assists", query_result["player"]["stats"]["TNTGames"])
    wither_class["deaths"] = getValue("new_witherwizard_deaths", query_result["player"]["stats"]["TNTGames"])
    wither_class["explosion"] = getValue("new_witherwizard_explode", query_result["player"]["stats"]["TNTGames"])
    wither_class["regeneration"] = getValue("new_witherwizard_regen", query_result["player"]["stats"]["TNTGames"])
    wither_class["prestige"] = str("new_witherwizard_prestige_field" in query_result["player"]["stats"]["TNTGames"])
    class_data.append(wither_class)
    kinetic_class = {}
    kinetic_class["class"] = "kinetic"
    kinetic_class["kills"] = getValue("new_kineticwizard_kills", query_result["player"]["stats"]["TNTGames"])
    kinetic_class["assists"] = getValue("new_kineticwizard_assists", query_result["player"]["stats"]["TNTGames"])
    kinetic_class["deaths"] = getValue("new_kineticwizard_deaths", query_result["player"]["stats"]["TNTGames"])
    kinetic_class["explosion"] = getValue("new_kineticwizard_explode", query_result["player"]["stats"]["TNTGames"])
    kinetic_class["regeneration"] = getValue("new_kineticwizard_regen", query_result["player"]["stats"]["TNTGames"])
    kinetic_class["prestige"] = str(
        "new_kineticwizard_prestige_field" in query_result["player"]["stats"]["TNTGames"])
    class_data.append(kinetic_class)
    blood_class = {}
    blood_class["class"] = "blood"
    blood_class["kills"] = getValue("new_bloodwizard_kills", query_result["player"]["stats"]["TNTGames"])
    blood_class["assists"] = getValue("new_bloodwizard_assists", query_result["player"]["stats"]["TNTGames"])
    blood_class["deaths"] = getValue("new_bloodwizard_deaths", query_result["player"]["stats"]["TNTGames"])
    blood_class["explosion"] = getValue("new_bloodwizard_explode", query_result["player"]["stats"]["TNTGames"])
    blood_class["regeneration"] = getValue("new_bloodwizard_regen", query_result["player"]["stats"]["TNTGames"])
    blood_class["prestige"] = str("new_bloodwizard_prestige_field" in query_result["player"]["stats"]["TNTGames"])
    class_data.append(blood_class)
    toxic_class = {}
    toxic_class["class"] = "toxic"
    toxic_class["kills"] = getValue("new_toxicwizard_kills", query_result["player"]["stats"]["TNTGames"])
    toxic_class["assists"] = getValue("new_toxicwizard_assists", query_result["player"]["stats"]["TNTGames"])
    toxic_class["deaths"] = getValue("new_toxicwizard_deaths", query_result["player"]["stats"]["TNTGames"])
    toxic_class["explosion"] = getValue("new_toxicwizard_explode", query_result["player"]["stats"]["TNTGames"])
    toxic_class["regeneration"] = getValue("new_toxicwizard_regen", query_result["player"]["stats"]["TNTGames"])
    toxic_class["prestige"] = str("new_toxicwizard_prestige_field" in query_result["player"]["stats"]["TNTGames"])
    class_data.append(toxic_class)
    return {"wizard_data" : wizard_data, "class_data" : class_data}